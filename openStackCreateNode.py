
# Imports
from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver
import libcloud.security
import json

# Config
with open('config/config-dev.json') as json_data_file:
    config = json.load(json_data_file)

libcloud.security.VERIFY_SSL_CERT = False

OpenStack = get_driver(Provider.OPENSTACK)
driver = OpenStack(config['auth_username'],
                   config['auth_password'],
                   ex_force_auth_version=config['auth_version'],
                   ex_force_auth_url=config['auth_url'],
                   ex_tenant_name=config['project_name'],
                   ex_force_service_name=config['service_name'])


#
# Create the Node
#
images = driver.list_images()
sizes = driver.list_sizes()
networks = driver.ex_list_networks()

node = driver.create_node(name='test', image=images[0], size=sizes[2], networks=[networks[0]])
