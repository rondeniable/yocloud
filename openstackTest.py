
# Imports
from pprint import pprint
from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver
import libcloud.security
import json

# Config
with open('config/config-dev.json') as json_data_file:
    config = json.load(json_data_file)

libcloud.security.VERIFY_SSL_CERT = False

OpenStack = get_driver(Provider.OPENSTACK)
driver = OpenStack(config['auth_username'],
                   config['auth_password'],
                   ex_force_auth_version=config['auth_version'],
                   ex_force_auth_url=config['auth_url'],
                   ex_tenant_name=config['project_name'],
                   ex_force_service_name=config['service_name'])

#
# Start using the driver
#

# Print out the list of nodes
pprint("Nodes:")
pprint(driver.list_nodes())

# Print out the list of Images
pprint("Images:")
pprint(driver.list_images())

# Print out the sizes
pprint("Sizes")
pprint(driver.list_sizes())
