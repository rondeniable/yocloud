#
# Cookbook Name:: yocloud
# Attributes:: yocloud
#
# Copyright (C) 2014
#
#
default['environment'] = 'prod'
default['artifact'] = {
    "name": "yodlee",
    "hostname": "001-source-ias",
    "server": "source.yc01.yodlee.com",
    "serveralias": "artifact.yc01.yodlee.com",
    "ip_address": "192.168.226.209",
    "url": "/yum",
    "proto": "http",
    "description": "Yodlee Artifact Repository Server",
    "api_user": "br",
    "api_pass": "Y0dlee!",
    "yum_repositories": [
        {
            "name": "centos-base-7-prod-yodlee",
            "description": "CentOS 7 Base",
            "enabled": "TRUE",
            "url": "/yum"
        },
        {
            "name": "centos-epel-7-prod-yodlee",
            "description": "CentOS 7 EPEL",
            "enabled": "TRUE",
            "url": "/yum"
        },
        {
            "name": "centos-extras-7-prod-yodlee",
            "description": "CentOS 7 Extras",
            "enabled": "TRUE",
            "url": "/yum"
        },
        {
            "name": "centos-updates-7-prod-yodlee",
            "description": "CentOS 7 Base",
            "enabled": "FALSE",
            "url": "/yum"
        },
        {
            "name": "centos-other-7-prod-yodlee",
            "description": "CentOS 7 Base",
            "enabled": "TRUE",
            "url": "/yum"
        }
    ],
    "gem_repository": {
        "name": "rubygems-prod-yodlee",
        "hostname": "rubygems",
        "serveralias": "rubygems.yc01.yodlee.com",
        "description": "CentOS 7 Ruby Gems",
        "enabled": "TRUE",
        "url": "/api/",
        "port": "80"
    },
    "pypi_repository": {
        "name": "pypi-prod-yodlee",
        "hostname": "pypi",
        "serveralias": "pypi.yc01.yodlee.com",
        "description": "CentOS 7 Python",
        "enabled": "TRUE",
        "url": "/root/pypi/",
        "port": "80"
    },
    "imaging_repository": {
        "name": "imaging-prod-yodlee",
        "description": "Instance Base Image Library",
        "enabled": "TRUE",
        "url": "/images",
        "port": "80"
    },
    "use_proxy": 0
}

default['openstack']['region'] = 'us-west-03'
default['openstack'] = {
    "hosts": [
        {
            "name": "cntrl01.prod.us-south-01.yodlee.cloud",
            "type": "",
            "ip_address": "",
            "region": "us-west-03",
            "chef_role": "role_pcntrl_ias"
        },
        {
            "name": "cntrl02.prod.us-west-03.yodlee.cloud",
            "type": "",
            "ip_address": "",
            "region": "us-west-03",
            "chef_role": "role_pcntrl_ias"
        },
        {
            "name": "nova001.prod.us-west-03.yodlee.cloud",
            "region": "us-west-03",
            "type": "",
            "ip_address": "172.17.51.103",
            "management_ip_address": "172.17.51.103",
            "management_interface": "eth3",
            "ext_bridge": "br-ex",
            "ext_interface_type": "bonded",
            "ext_interface": "bond0",
            "ext_interface_primary": "eth3",
            "ext_interface_secondary": "eth4",
            "availability_zone": "nova",
            "chef_role": "role_pnova_ias"
        }
    ],
    "clouds": [
        {
            "region": "us-west-03",
            "type": "openstack",
            "management_vip": "10.164.69.132",
            "auth_url": "http://10.164.69.132:5000/v3",
            "novncproxy_base_url": "https://10.164.69.132:6080/vnc_auto.html",
            "flavors": [
                {
                    "id": "100",
                    "name": "sp1.small",
                    "mem": "8192",
                    "disk": "200",
                    "cpu": "4"
                },
                {
                    "id": "101",
                    "name": "sp1.medium",
                    "mem": "16384",
                    "disk": "300",
                    "cpu": "4"
                },
                {
                    "id": "102",
                    "name": "sp1.large",
                    "mem": "32768",
                    "disk": "200",
                    "cpu": "4"
                },
                {
                    "id": "103",
                    "name": "sp2.medium",
                    "mem": "16384",
                    "disk": "300",
                    "cpu": "6"
                }
            ],
            "images": [],
            "networks": [],
            "tenants": {
                "admin": {
                    "username": "build",
                    "api_key": "!Pr0dY0dlee!"
                },
                "services_prod_ias": {
                    "username": "build",
                    "api_key": "!Pr0dY0dlee!"
                },
                "services_prod_ypa": {
                    "username": "build",
                    "api_key": "!Pr0dY0dlee!"
                },
                "gatherer_prod_ypa": {
                    "username": "build",
                    "api_key": "!Pr0dY0dlee!"
                },
                "services_splunk_ias": {
                    "username": "build",
                    "api_key": "!Pr0dY0dlee!"
                }
            },
            "services": [
                {
                    "type": "rabbit_mq",
                    "hosts": "10.112.241.66:5672",
                    "userid": "openstack",
                    "password": "d5045a4fdc3ee4229be1"
                },
                {
                    "type": "keystone",
                    "auth_uri_port": "5000",
                    "auth_url_port": "35357",
                    "auth_plugin": "password",
                    "project_domain_id": "default",
                    "user_domain_id": "default",
                    "project_name": "service"
                },
                {
                    "type": "nova",
                    "auth_strategy": "keystone",
                    "username": "nova",
                    "password": "79d4f1b797e5189088ac"
                },
                {
                    "type": "neutron",
                    "auth_strategy": "keystone",
                    "admin_tenant_name": "service",
                    "admin_username": "neutron",
                    "admin_password": "aff3abd416caac78e28d"
                },
                {
                    "type": "glance",
                    "host": "10.112.241.66"
                },
                {
                    "type": "ml2",
                    "type_drivers": "flat,vlan,gre,vxlan",
                    "tenant_network_types": "gre",
                    "network_vlan_ranges": "",
                    "mechanism_drivers": "openvswitch"
                }
            ],
            "availability_zones": [],
            "host_aggregates_zones": []
        }
    ]
}