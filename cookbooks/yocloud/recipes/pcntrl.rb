#
# Cookbook Name:: yocloud
# Recipe:: pcntrl
#
# Copyright 2015, Yodlee
#
# All rights reserved - Do Not Redistribute
#

# Include Base Cloud Items
include_recipe 'yocloud::default'

#
# Let's Setup Yum Repositories
#

#
# Install required packages
#
yum_package 'libvirt-client.x86_64'
yum_package 'libvirt'
yum_package 'openstack-nova-compute'
yum_package 'sysfsutils'
yum_package 'openstack-neutron'
yum_package 'openstack-neutron-ml2'
yum_package 'openstack-neutron-openvswitch'
yum_package 'net-tools'