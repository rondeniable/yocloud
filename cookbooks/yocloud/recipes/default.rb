#
# Cookbook Name:: services-cloud-ias
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

#
# Load all the hosts for this cloud
#
$hosts=node['default']['openstack']['cloud_ias']['hosts'].select { |h| "#{h['region']}" == node['default']['openstack']['region'] }

#
# Make all openstack node entries in /etc/hosts
#
$hosts.each do |host|

  # Create Host File Entrie
  hostsfile_entry "#{host['ip_address']}" do
    hostname "#{host['name']}"
    action :create
  end

end

#
# Install Yodlee Custom Yum repos containing Chef RPMs
#
repo = node['default']['artifact_ias']
yum_repository "centos-rdo-7-#{node['default']['environment']}-yodlee" do
  name "centos-rdo-7-#{node['default']['environment']}-yodlee"
  description "#{repo['description']} Centos"
  baseurl "#{repo['proto']}://#{repo['ip_address']}#{repo['url']}/centos-rdo-7-#{node['default']['environment']}-yodlee/"
  sslverify FALSE
  gpgcheck FALSE
  enabled TRUE
  action :create
end

# Install iotop
yum_package 'iotop'

# Check ntp skew. If too large, stop ntp, run ntpdate and then restart ntp
execute "manual ntpdate for large skew" do
  command "/bin/systemctl stop ntpd && /sbin/ntpdate #{node['ntp']['servers'][0]} && systemctl start ntpd || systemctl start ntpd"
  only_if do
    if node['ntp']['servers'][0].match(/^([0-9]{1,3}.){3}[0-9]{1,3}$/)
      # Don't resolve hostnames, if I've got an IP
      offset = `/sbin/ntpq -pn | grep #{node['ntp']['servers'][0]}`.split(" ")[8].to_i.abs
    else
      # I don't have an IP, so resolve hostnames
      offset = `/sbin/ntpq -p | grep #{node['ntp']['servers'][0]}`.split(" ")[8].to_i.abs
    end
    result = offset > 100 ? true : false
    result
  end
end
