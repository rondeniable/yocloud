name             'role_pnova_ias'
maintainer       'Yodlee'
maintainer_email 'rdavis@yodlee.com'
license          'All rights reserved'
description      'Installs/Configures role_pnova_ias'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
depends 	       'chef-client'
depends          'yocloud'